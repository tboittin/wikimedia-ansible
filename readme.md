# Instructions
- Faire une cible simple
- nouvel inventaire
- nouveau playbook

### ajouter la clef publique dans authorized_keys du client
$ vi .ssh/authorized_keys

### coller la clef publique 
on a repris le master donc ansible est deja installe
on utilise la doc : https://www.mediawiki.org/wiki/Manual:Running_MediaWiki_on_Debian_or_Ubuntu/fr
dans les requirements de MediaWiki, il est stipule qu'il a besoin de:
- Apache
- PHP
- Mariadb

### on cree le fichier de base
$ vi /etc/ansible/wikimedia.yml

---
- name: "wikimedia"
  hosts: all

  tasks:

    - name: "install wiki tools" # on installe les outils
      apt:
        name: "{{ item }}"
        state: present
      with_items:
      - apache2
      - mariadb-server
      - php
      - php-mysql
      - libapache2-mod-php
      - php-xml
      - php-mbstring
      - python3-pymysql # necessaire pour faire fonctionner la bdd
      - mediawiki

    - name: "lancement apache"
      service:
        name: apache2
        state: started
        enabled: yes

    - name: "lancement mysql"
      service:
        name: mysql
        state: started
        enabled: yes

    - name: "creer user"
      mysql_user:
        login_unix_socket: /var/run/mysqld/mysqld.sock
        name: db_thom
        password: 12345
        priv: '*.*:ALL'
        state: present

    - name: "creer db"
      mysql_db:
        login_unix_socket: /var/run/mysqld/mysqld.sock
        name: my_db
        state: present

    - name: "setting php upload_max_filesize"
      lineinfile:
        path: /etc/php/7.4/apache2/php.ini
        regexp: 'upload_max_filesize'
        line: upload_max_filesize = 20M

    - name: "setting php memory_limit"
      lineinfile:
        path: /etc/php/7.4/apache2/php.ini
        regexp: 'memory_limit'
        line: memory_limit = 128M

    - name: "lien symbolique wiki"
      file:
        src: /var/lib/mediawiki
        dest: /var/www/html/mediawiki
        state: link

# Avec des roles
### En creant des roles, on va scinder le yml en role et repartir les parties dans les branches creees avec ansible-galaxy

---
- name: "wikimedia"
  hosts: all
  roles:
    - mediawiki-tools
    - mediawiki-services
    - mediawiki-mysql
    - mediawiki-lien

